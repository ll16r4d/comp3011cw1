import requests, sys, os
from getpass import getpass
from tabulate import tabulate

api = "http://127.0.0.1:8000"
token = ""

def log():
    return '0c9d4893f65ad67ab08de16dd673c98e8e1b1ce2'

def login(url):
    if api == "http://127.0.0.1:8000":
        username = input("Username: ")
        email = input("Email: ")
        password = getpass("Password: ")

        login = {'username': username,
                'email':email,
                'password':password}

        request = requests.post(api + "/rest-auth/login/", data = login)
        if request.status_code == 200:
            token_dict = request.json()
            token = token_dict["key"]
            return token
        else:
            print("Incorrect credentials")
            return None

def register():
    username = input("Username: ")
    email = input("Email: ")
    password = getpass("Password: ")
    confirm_pass = getpass("Confirm Password: ")

    register = {'username': username,
            'email': email,
            'password1': password,
            'password2': confirm_pass}

    request = requests.post(api + "/rest-auth/registration/", data = register)

    if request.status_code == 201:
        token_dict = request.json()
        token = token_dict["key"]
        return token
    else:
        print("Error: Something went wrong")
        return None

def logout():
    request = requests.post(api + "/rest-auth/logout/")
    if request.status_code == 200:
        token = ""
        print("Logged out successfully")
    else:
        print("Error: not logged in")

def retrieve_data(link):
    data = {'Authorization': "Token " + token}
    return requests.get(link, headers={"Authorization": "Token " + str(token)}, data=data).json()

def module_list():
    modules = retrieve_data(api + "/module_instances")
    module_list = []
    for m in modules:
        module = retrieve_data(m['module'])
        module_id = module['module_id']
        module_name = module['module_name']
        year = m['year']
        semester = m['semester']
        lecturers = ''
        for l in m['teaching_lecturers']:
            lecturer = retrieve_data(l)
            lecturer_id = lecturer['lecturer_id']
            lecturer_name = lecturer['lecturer_name']
            lecturers = lecturers + lecturer_id + ", " + lecturer_name + '\n'
        module_list.append([module_id, module_name, year, semester, lecturers])
    print(tabulate(module_list, headers=['Code', 'Name', 'Year', 'Semester', 'Taught By']))

def view_ratings():
    lecturer = retrieve_data(api + "/lecturers")
    for l in lecturer:
        stars = "*" * int(l['total_rating'])
        print("the rating of professor " + l['lecturer_name'] + " (" + l['lecturer_id'] + ") is " + stars)

def average():
    None

def rate(command):
    professor_id = command[1]
    module_code = command[2]
    year = command[3]
    semester = command[4]
    rating = command[5]

    module = str(api + '/modules/' + str(module_code) + '/')
    lecturer = str(api + '/lecturers/' + str(professor_id) + '/')

    rating =   {'lecturer': lecturer,
                'module': module,
                'year': year,
                'semester': semester,
                'rating': rating}

    request = requests.post(api + "/ratings/", data = rating)

while True:
    command = input("\nll16r4d_API >>> ")
    command = command.split()

    if command[0] == "login":
        if len(command) == 2:
            token = login(command[1])
            print(token)
        else:
            print('Error: Inappropriate number of arguments given')
    elif command[0] == "log":
        token = log()

    elif command[0] == "register":
        token = register()
        print(token)

    elif command[0] == "logout":
        logout()

    elif command[0] == "list":
        module_list()

    elif command[0] == "view":
        view_ratings()

    elif command[0] == "average":
        average()

    elif command[0] == "rate":
        if len(command) == 6:
            rate(command)
        else:
            print('Error: Inappropriate number of arguments given')

    elif command[0] == "exit":
        print('Exitting')
        break

    else:
        print("Unrecognised command")
