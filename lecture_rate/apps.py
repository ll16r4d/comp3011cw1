from django.apps import AppConfig


class LectureRateConfig(AppConfig):
    name = 'lecture_rate'
