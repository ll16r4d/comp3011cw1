from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Lecturer, Module, Rating, ModuleInstance

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'password']

class LecturerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Lecturer
        fields = ['lecturer_id', 'lecturer_name', 'total_rating']

class ModuleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Module
        fields = ['module_id', 'module_name']

class ModuleInstanceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ModuleInstance
        fields = ['instance_id', 'module', 'teaching_lecturers', 'year', 'semester']

class RatingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Rating
        fields = ['lecturer', 'module', 'year', 'semester', 'rating']
