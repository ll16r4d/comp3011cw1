from django.db import models
import datetime
from django.contrib.auth.models import User

RATINGS = [
    (0, 0),
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5)
]

YEARS = []
for year in range(2015, (datetime.datetime.now().year+2)):
    YEARS.append((year, year))

SEMESTERS = [
    (1, 1),
    (2, 2)
]

class Lecturer(models.Model):
    lecturer_id = models.CharField(primary_key=True, max_length=5, default = "0")
    lecturer_name = models.CharField(max_length = 50)
    total_rating = models.IntegerField(choices = RATINGS, blank = True, default = 0)

    def __str__(self):
        return str(self.lecturer_name)

class Module(models.Model):
    module_id = models.CharField(primary_key=True, max_length=4)
    module_name = models.CharField(max_length = 50)

    def __str__(self):
        return str(self.module_name)

class ModuleInstance(models.Model):
    instance_id = models.AutoField(primary_key=True)
    module = models.ForeignKey('Module', on_delete=models.CASCADE)
    teaching_lecturers = models.ManyToManyField('Lecturer')
    year = models.IntegerField(choices = YEARS)
    semester = models.IntegerField(choices = SEMESTERS)

    def __str__(self):
        return str(str(self.module) + " in semester " + str(self.semester) + " in " + str(self.year))


class Rating(models.Model):
    rating_id = models.AutoField(primary_key=True)
    module = models.ForeignKey('Module', on_delete=models.CASCADE)
    lecturer = models.ForeignKey('Lecturer', on_delete=models.CASCADE)
    year = models.IntegerField(choices = YEARS)
    semester = models.IntegerField(choices = SEMESTERS)
    rating = models.IntegerField(choices = RATINGS)

    def __str__(self):
        return str(str(self.module) + " taught by " + str(self.lecturer) + " was rated " + str(self.rating))
