from django.contrib import admin
from .models import Lecturer, Module, Rating, ModuleInstance

admin.site.register(Lecturer)
admin.site.register(Module)
admin.site.register(Rating)
admin.site.register(ModuleInstance)

# Register your models here.
