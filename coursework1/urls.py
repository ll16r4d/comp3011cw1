"""coursework1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from rest_framework import routers, serializers, viewsets
from lecture_rate import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'lecturers', views.LecturerViewSet)
router.register(r'modules', views.ModuleViewSet)
router.register(r'module_instances', views.ModuleInstanceSet)
router.register(r'ratings', views.RatingViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
    path('admin/', admin.site.urls),

    url(r'ratings', views.RatingViewSet.as_view({'get': 'list', 'post': 'create'}), name = 'ratings')
]
